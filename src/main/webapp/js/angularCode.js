var app = angular.module('app', []);


app.controller('postController', function($scope, $http, $location) {
    $scope.submitForm = function(){
        var url = $location.absUrl() + "/postcustomer";

        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            userName: $scope.username,
            email: $scope.email,
            phoneNo: $scope.phoneno,
            password: $scope.password
        };


        $http.post(url, data, config).then(function (response) {
            $scope.postResultMessage = "Sucessful!";
        }, function (response) {
            $scope.postResultMessage = "Fail!";
        });

        $scope.username = "";
        $scope.email = "";
        $scope.phoneno ="";
        $scope.password ="";
    }
});

app.controller('postAppoinmentCtr', function($scope, $http, $location) {

    $scope.submitForm = function(){
        var url = $location.absUrl() + "/addAppoinment";

        var dateVal = document.getElementById("dateValue").value;
        var timeVal = document.getElementById("timeValue").value;

        $scope.date = dateVal;
        $scope.time = timeVal;


        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        var data = {
            type: $scope.type,
            date: $scope.date,
            time: $scope.time,
            description: $scope.description
        };


        $http.post(url, data, config).then(function (response) {
            $scope.postResultMessage = "Sucessful!";
        }, function (response) {
            $scope.postResultMessage = "Fail!";
        });

        $scope.type = "";
        $scope.dateX = "";
        $scope.timeX ="";
        $scope.description ="";

    }
});
app.controller('testController', function($scope, $window) {

    alert("Hi test");
    $scope.Customers = [
        { Name: "John Hammond", Country: "United States" },
        { Name: "Mudassar Khan", Country: "India" },
        { Name: "Suzanne Mathews", Country: "France" },
        { Name: "Robert Schidner", Country: "Russia" }
    ];
    $scope.GetDetails = function (index) {
        var name = $scope.Customers[index].Name;
        var country = $scope.Customers[index].Country;
        $window.alert("Name: " + name + "\nCountry: " + country);
    };
});
app.controller('getallcustomersController', function($scope, $http, $location) {
   /* alert("Hi");*/
    $scope.showAllCustomers = false;

    $scope.getAllCustomers = function() {
        var url = $location.absUrl() + "/allCustomerDetails";
        /*alert(url);
        alert("hi");*/
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }

        $http.get(url, config).then(function(response) {
            alert(response.data.status);
            if (response.data.status == "Done") {
                $scope.allcustomers = response.data;

                $scope.showAllCustomers = true;
            } else {
                $scope.getResultMessage = "get All Customers Data Error!";
            }

        }, function(response) {

            $scope.getResultMessage = "Fail!";
        });
       /* alert("Hi3");*/
    }
});

app.controller('getcustomerController', function($scope, $http, $location) {

    $scope.showCustomer = false;

    $scope.getCustomer = function() {
        var url = $location.absUrl() + "customer/" + $scope.customerId;

        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }

        $http.get(url, config).then(function(response) {

            if (response.data.status == "Done") {
                $scope.customer = response.data;
                $scope.showCustomer = true;

            } else {
                $scope.getResultMessage = "Customer Data Error!";
            }

        }, function(response) {
            $scope.getResultMessage = "Fail!";
        });

    }
});

app.controller('getcustomersbylastnameController', function($scope, $http, $location) {

    $scope.showCustomersByLastName = false;

    $scope.getCustomersByLastName = function() {
        var url = $location.absUrl() + "findbylastname";

        var config = {
            headers : {	'Content-Type' : 'application/json;charset=utf-8;' },

            params: { 'lastName' : $scope.customerLastName }
        }

        $http.get(url, config).then(function(response) {

            if (response.data.status == "Done") {
                $scope.allcustomersbylastname = response.data;
                $scope.showCustomersByLastName = true;

            } else {
                $scope.getResultMessage = "Customer Data Error!";
            }

        }, function(response) {
            $scope.getResultMessage = "Fail!";
        });

    }
});

app.controller('getcustomersbylastnameController', function($scope, $http, $location) {
    alert("Hi");
});

/*Get ALL Customer Details*/
app.controller('allCustomerDetails', function($scope, $http, $location) {
    $scope.listCustomers = [];

    // $scope.getAllCustomer =
    function getAllCustomer(){
        // get URL
        var url = $location.absUrl() + "/allCustomerDetails";
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }
        // do getting
        /*$http.get(url).then(function (response) {
            $scope.getDivAvailable = true;
            $scope.listCustomers = response.data;
        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });*/

        $http.get(url, config).then(function(response) {
            $scope.getDivAvailable = true;
            $scope.listCustomers = response.data;

        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });
    }

    getAllCustomer();
});

/*Get all appoinment details by user id*/
app.controller('allAppoinmentDetails', function($scope, $http, $location) {
    $scope.listAppoinment = [];

    // $scope.getAllCustomer =
    function getAllCustomer(){
        // get URL
        var url = $location.absUrl() + "/getAllAppoinmentDetails";
        var config = {
            headers : {
                'Content-Type' : 'application/json;charset=utf-8;'
            }
        }
        // do getting
        /*$http.get(url).then(function (response) {
            $scope.getDivAvailable = true;
            $scope.listCustomers = response.data;
        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });*/

        $http.get(url, config).then(function(response) {
            $scope.getDivAvailable = true;
            $scope.listAppoinment = response.data;

        }, function error(response) {
            $scope.postResultMessage = "Error Status: " +  response.statusText;
        });
    }

    getAllCustomer();
});


/*app.directive('ngUnique', ['$http', function (async) {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {

            elem.on('blur', function (evt) {
                scope.$apply(function () {
                    var val = elem.val();
                    var req = { "userName": val }
                    var url = $location.absUrl() + "/getAllAppoinmentDetails";
                    var config = {
                        headers : {
                            'Content-Type' : 'application/json;charset=utf-8;'
                        }
                    }

                    $http.get(url, config).then(function(response) {
                        $scope.getDivAvailable = true;
                        $scope.listAppoinment = response.data;

                    }, function error(response) {
                        $scope.postResultMessage = "Error Status: " +  response.statusText;
                    });
                });
            });
        }
    }
}]);*/

