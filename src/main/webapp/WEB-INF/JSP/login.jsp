<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE html>

<head>
    <meta charset="UTF-8" />
    <title>Login and Registration Form with HTML5 and CSS3</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/logincss.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>
<body>
<div class="container">

    <header>
        <h1>Beauty Salon</h1>

    </header>
    <section>
        <div id="container_demo"  style="float: right; margin-right: 100px">
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">
                <div id="login" class="animate form">
                    <form action="${path}/login" method="post">
                        <h1>Log in</h1>
                        <p>
                            <label for="username" class="uname" > Your email or username </label>
                            <input id="username" name="username" required="required" type="text"/>
                        </p>
                        <p>
                            <label for="password" class="youpasswd"> Your password </label>
                            <input id="password" name="password" required="required" type="password"/>
                        </p>
                        <p class="keeplogin">
                            <input type="checkbox" name="remember-me" id="remember-me" value="remember-me" />
                            <label for="remember-me">Keep me logged in</label>
                        </p>
                        <p class="login button">
                            <input type="submit" value="Sign in"/>
                        </p>
                        <p class="change_link">
                            <a href="#">Foget Password &nbsp; &nbsp; &nbsp;</a>
                            <a href="regForm" class="to_register">sign up</a>
                        </p>
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
</body>
</html>






