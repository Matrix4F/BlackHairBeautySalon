

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>
<script
        src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
<script type="text/javascript" src="${path}/js/angularCode.js"></script>

<body>


<div class="page-header" >
    <h1 class="page-title">Appointment</h1>
    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li class="active">Appointment</li>

    </ol>

    <div class="page-header-actions">

    </div>
</div>
<div class="page-content" ng-app="app">
    <!-- Panel Table Individual column searching -->
    <div class="panel">
        <header class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="panel-title">Appointment &nbsp;<span class="tooltip-dark" data-toggle="tooltip" data-original-title="Help tips" data-trigger="focus / hover"><button type="button" class="btn-info popover-info btn-sm btn-icon btn-inverse btn-info btn-round" data-content="Can accept or reject any applications for selection test in this table. And can search any content of the table. And also can view fully application through the id." data-toggle="popover" data-original-title="About this table..." data-trigger="click"><i class="grey-500 icon wb-info"></i></button></span></h4>
                </div>
                <%--<Add apointment button>--%>
                <div class="col-md-6" >
                    <br/>
                    <div class="col-md-2 col-md-offset-7">
                        <input type="button" class="btn btn-primary" value="Add New Appoinment"/>
                    </div>
                </div>
            </div>
        </header>
        <div style="text-align: center">

        </div>
        <div ng-controller="allAppoinmentDetails" class="panel-body">

                <table  class="table table-hover dataTable table-striped width-full" id="exampleTableSearch">
                    <thead>

                    Search by anything here: <input type="text" ng-model="searchText">

                    <tr ng-repeat="appoinment in listAppoinment | filter:searchText">
                        <td>{{appoinment.type}}</td>
                        <td>{{appoinment.date}}</td>
                        <td>{{appoinment.time}}</td>
                        <%--<td>{{appoinment.description}}</td>--%>
                        <%-- <td><span class="label label-success">Approved</span></td>--%>
                        <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span>Approved</button></p></td>
                        <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span>Edit</button></p></td>
                        <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>
                    </tr>
                    </tbody>
                </table>

        </div>
    </div>
</div>


<!-- End Page -->

<!-- Footer -->
<%@include file="includes/includejs.jsp" %>

<!--Confirm box create area for accept button-->
<script>

    function assignConfirmationToAccept() {
        if (confirm("Application will be Accepted and can't undo this operation!. Do you want to Approve this Record...?")) {

            return true;
        } else
        {
            return false;
        }

    }
</script>

<!--Confirm box create area for reject button-->
<script>

    function assignConfirmationToReject() {
        if (confirm("Application will be Rejected and can't undo this operation!. Do you want to Reject this Record ?")) {
            return true;
        } else
        {
            return false;
        }

    }
</script>

<script>
    $(document).ready(function () {
        $('.acrebtn').delegate('.accept', 'click', function (e) {

            var confirm = assignConfirmationToAccept();
            if (confirm === true) {
                var Id = jQuery(this).find('input').val(); //getting input button value for accept button
                var decision = "approved";

                $.ajax({url: 'ApplicationServlet?id=' + Id + '&decision=' + decision, success: function (data) {

                        $("?accept").html(data);
                        window.location = "applications.jsp";
//                        window.location.reload(true);

                    }});
                // e.preventDefault();
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('.acrebtn').delegate('.reject', 'click', function (e) {

            var confirm = assignConfirmationToReject();
            if (confirm === true) {
                var Id = jQuery(this).find('input').val(); //getting input button value for reject button
                var decision = "rejected";

                $.ajax({url: 'ApplicationServlet?id=' + Id + '&decision=' + decision, success: function (data) {

                        $("?reject").html(data);
                        window.location = "applications.jsp";
//                        window.location.reload(true);

                    }});
                // e.preventDefault();
            }
        });
    });
</script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;

        $(document).ready(function ($) {
            Site.run();
        });

        // Individual column searching
        // ---------------------------
        (function () {
            $(document).ready(function () {
                var defaults = $.components.getDefaults("dataTable");

                var options = $.extend(true, {}, defaults, {
                    initComplete: function () {
                        this.api().columns().every(function () {
                            var column = this;
                            var select = $(
                                '<select class="form-control width-full" data-plugin="selectpicker"><option value=""></option></select>'
                            )
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '',
                                            true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (
                                d, j) {
                                select.append('<option value="' + d +
                                    '">' + d + '</option>');
                            });
                        });
                    }
                });

                $('#exampleTableSearch').DataTable(options);
            });
        })();

        // Table Tools
        // -----------
        (function () {
            $(document).ready(function () {
                var defaults = $.components.getDefaults("dataTable");

                var options = $.extend(true, {}, defaults, {
                    "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [-1]
                    }],
                    "iDisplayLength": 5,
                    "aLengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
                    "oTableTools": {
                        "sSwfPath": "assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
                    }
                });

                $('#exampleTableTools').dataTable(options);
            });

        })();

    })(document, window, jQuery);
</script>

</body>

</html>

