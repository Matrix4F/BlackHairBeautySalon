<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>
<html lang="en">

<!-- Mirrored from lithemes.com/blackair/v2/white/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Apr 2018 20:13:45 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blackair - One Page Parallax, Responsive HTML5, CSS3, Bootstrap 3+ Template for Hairdressers, Hair Salons
        &amp; Stylists</title>
    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/master.css">

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>--%>

    <%--<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>--%>
    <%--<link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/s.cdpn.io/148866/reset.css">--%>

    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>--%>



    <%--<link rel="stylesheet" href="css/signin.css">--%>

    <%--<script src="js/signinjs.js"></script>--%>

    <%--popup login--%>
    <%--<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">--%>

    <!--[if lt IE 9]>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>



    <c:url var="logoutUrl" value="/login" />
    <form action="${logoutUrl}" method="post" id="logoutForm">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form>

<div id="overlay"></div>
<div id="mobile-menu">
    <ul>
        <li class="page-scroll"><a href="#salon">The SalonX</a></li>
        <li class="page-scroll"><a href="#ourteam">Meet our team</a></li>
        <li class="page-scroll"><a href="#services">Our Services</a></li>
        <li class="page-scroll"><a href="#gallery">portfolio</a></li>
        <li class="page-scroll"><a href="#promotions">Promotions</a></li>
        <li class="page-scroll"><a href="#video">Video</a></li>
        <li><a href="blog.html">Blog</a></li>
        <li><a href="blog-classic.html">Blog Classic</a></li>
        <li><a href="blog-fullwidth.html">Blog Full Width</a></li>
        <li><a href="blog-masonary.html">Blog Masonary</a></li>
        <li class="page-scroll"><a href="#contact">Contact</a></li>
    </ul>
</div>
<div id="page">
    <header id="pagetop">
        <nav class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="logo page-scroll"><a href="#pagetop"><img src="images/logo.png" alt="logo"></a>
                        </div>
                        <div class="mm-toggle-wrap">
                            <div class="mm-toggle"><i class="icon-menu"><img src="images/menu-icon.png" alt="Menu"></i>
                            </div>
                        </div>
                        <ul class="menu">
                            <%--<li class="page-scroll"><a href="#salon">The SalonX</a></li>--%>
                            <li class="page-scroll"><a href="#ourteam">Meet our team</a></li>
                            <li class="page-scroll"><a href="#services">Our Services</a></li>
                            <li class="page-scroll"><a href="#gallery">portfolio</a></li>
                            <li class="page-scroll"><a href="#promotions">Promotions</a></li>
                            <li class="page-scroll"><a href="#video">Video</a></li>
                            <li><a href="blog.html">Blog</a>
                                <ul>
                                    <li><a href="blog-classic.html">Blog Classic</a></li>
                                    <li><a href="blog-fullwidth.html">Blog Full Width</a></li>
                                    <li><a href="blog-masonary.html">Blog Masonary</a></li>
                                </ul>
                            </li>
                            <li><a class="cd-signup" href="regForm">AddUser</a></li>
                            <li class="page-scroll"><a href="#contact">Contact</a></li>
                            <c:if test="${pageContext.request.userPrincipal.name != null}">
                                <li><a class="cd-signup" href="main">Appointment</a></li>

                                <li role="presentation">
                                    <a href="#" onclick="javascript:logoutForm.submit();" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Logout</a>
                                </li>
                            </c:if>
                            <c:if test="${pageContext.request.userPrincipal.name == null}">
                                <li><a class="cd-signup" href="login">Sign In</a></li>
                            </c:if>








                            <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--%>

                            <%--<li class="" style="background: #0f3e68"><a href="#login" data-rel="popup" class="btn"><span--%>
                                    <%--style="color: #FFFFFF">Login</span></a></li>--%>


                            <%--Login Form--%>
                            <%--<div data-role="main" class="ui-content">--%>
                            <%--<div data-role="popup" id="login" class="ui-content" style="min-width:250px;">--%>
                                <%--<form method="post" action="#">--%>
                                    <%--<div>--%>
                                        <%--<h3>Login information</h3>--%>
                                        <%--<label for="usrnm" class="ui-hidden-accessible">Username:</label>--%>
                                        <%--<input type="text" name="user" id="usrnm" placeholder="Username">--%>
                                        <%--<label for="pswd" class="ui-hidden-accessible">Password:</label>--%>
                                        <%--<input type="password" name="passw" id="pswd" placeholder="Password">--%>
                                        <%--<label for="log">Keep me logged in</label>--%>
                                        <%--<input type="checkbox" name="login" id="log" value="1" data-mini="true">--%>
                                        <%--<input type="submit" data-inline="true" value="Log in">--%>
                                    <%--</div>--%>
                                <%--</form>--%>
                            <%--</div>--%>
                            <%--</div>--%>

                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </nav>
    </header>

    <%--<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->--%>
        <%--<div class="cd-user-modal-container"> <!-- this is the container wrapper -->--%>
            <%--<ul class="cd-switcher">--%>
                <%--<li><a href="#0">Sign in</a></li>--%>
                <%--<li><a href="#0">New account</a></li>--%>
            <%--</ul>--%>

            <%--<div id="cd-login"> <!-- log in form -->--%>
                <%--<form class="cd-form">--%>
                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-email" for="signin-email">E-mail</label>--%>
                        <%--<input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail">--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-password" for="signin-password">Password</label>--%>
                        <%--<input class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password">--%>
                        <%--<a href="#0" class="hide-password">Hide</a>--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<input type="checkbox" id="remember-me" checked>--%>
                        <%--<label for="remember-me">Remember me</label>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<input class="full-width" type="submit" value="Login">--%>
                    <%--</p>--%>
                <%--</form>--%>

                <%--<p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>--%>
                <%--<!-- <a href="#0" class="cd-close-form">Close</a> -->--%>
            <%--</div> <!-- cd-login -->--%>

            <%--<div id="cd-signup"> <!-- sign up form -->--%>
                <%--<form class="cd-form">--%>
                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-username" for="signup-username">Username</label>--%>
                        <%--<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username">--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-email" for="signup-email">E-mail</label>--%>
                        <%--<input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-password" for="signup-password">Password</label>--%>
                        <%--<input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">--%>
                        <%--<a href="#0" class="hide-password">Hide</a>--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<input type="checkbox" id="accept-terms">--%>
                        <%--<label for="accept-terms">I agree to the <a href="#0">Terms</a></label>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<input class="full-width has-padding" type="submit" value="Create account">--%>
                    <%--</p>--%>
                <%--</form>--%>

                <%--<!-- <a href="#0" class="cd-close-form">Close</a> -->--%>
            <%--</div> <!-- cd-signup -->--%>

            <%--<div id="cd-reset-password"> <!-- reset password form -->--%>
                <%--<p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>--%>

                <%--<form class="cd-form">--%>
                    <%--<p class="fieldset">--%>
                        <%--<label class="image-replace cd-email" for="reset-email">E-mail</label>--%>
                        <%--<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">--%>
                        <%--<span class="cd-error-message">Error message here!</span>--%>
                    <%--</p>--%>

                    <%--<p class="fieldset">--%>
                        <%--<input class="full-width has-padding" type="submit" value="Reset password">--%>
                    <%--</p>--%>
                <%--</form>--%>

                <%--<p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>--%>
            <%--</div> <!-- cd-reset-password -->--%>
            <%--<a href="#0" class="cd-close-form">Close</a>--%>
        <%--</div> <!-- cd-user-modal-container -->--%>
    <%--</div> <!-- cd-user-modal -->--%>

    <div class="sliderfull">
        <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
             data-alias="classicslider1"
             style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
            <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
            <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
                <ul>
                    <!-- SLIDE  -->
                    <li data-index="rs-16" data-transition="zoomout" data-slotamount="default"
                        data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"
                        data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                        data-saveperformance="off" data-title="Intro" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/slider/slider1.jpg" alt="slider" data-bgposition="center center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                             data-no-retina>
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-17" data-transition="zoomout" data-slotamount="default"
                        data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"
                        data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                        data-saveperformance="off" data-title="Intro" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/slider/slider2.jpg" alt="slider" data-bgposition="center center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                             data-no-retina>

                    </li>
                    <li data-index="rs-18" data-transition="zoomout" data-slotamount="default"
                        data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"
                        data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"
                        data-saveperformance="off" data-title="Intro" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="images/slider/slider3.jpg" alt="slider" data-bgposition="center center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
                             data-no-retina>
                    </li>
                </ul>
                <div class="tp-static-layers"></div>
                <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
            </div>
        </div>
    </div>
    <section class="slider-titile">
        <div class="container">
            <div class="row">
                <div class="col-lg-11 pull-right">
                    <div class="sliderarrow">
                        <a class="left rev-leftarrow">Left</a>
                        <a class="right rev-rightarrow">Right</a>
                    </div>
                    <div class="titile-bg">
                        <h1>The Kewl Salon</h1>
                    </div>
                    <div class="white-bg">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                            congue. Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non congue
                            aliquam, mauris massa consequat sem, ut laoreet nisi erat et lectus.Nam auctor nisi est, nec
                            tempus lacus viverra nec. Nullam cursus, neque non congue aliquam, mauris massa consequat
                            sem, ut laoreet nisi erat et lectus.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="salon" class="col-padtop wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-padright-none wow fadeInLeft"><img src="images/saloon-1.jpg"
                                                                                              alt="Saloon"/></div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-padleft-none wow fadeInRight"><img src="images/saloon-2.jpg"
                                                                                              alt="Saloon"/></div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-padleft-none wow fadeInRight"><img src="images/saloon-3.jpg"
                                                                                              alt="Saloon"/></div>
            </div>
        </div>
    </section>
    <section id="ourteam" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <h2>Meet our Expert Stylists</h2>
                    <div class="ourteamd">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue.
                            Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non congue aliquam,
                            mauris massa consequat sem, ut laoreet nisi erat et lectus. Nullam non neque eros.
                            Pellentesque nec vulputate eros.</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="responsive">
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-1.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Sara Anderson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-2.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Medona Johnson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-3.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>

                            <div class="team">
                                <h3>Andria Joseph</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-1.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Sara Anderson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-2.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Medona Johnson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-3.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>

                            <div class="team">
                                <h3>Andria Joseph</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-1.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Sara Anderson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-2.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Medona Johnson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-3.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>

                            <div class="team">
                                <h3>Andria Joseph</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-1.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Sara Anderson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-2.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>
                            <div class="team">
                                <h3>Medona Johnson</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="third-effect">
                                <img src="images/out-tem-pic-3.jpg" class="img-responsive" alt="Our Team">
                                <div class="mask">
                                    <a href="#">Facebook</a>
                                    <a href="#" class="twitter">Twitter</a>
                                </div>
                            </div>

                            <div class="team">
                                <h3>Andria Joseph</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <section id="services" class="col-padtop">
        <div class="container">
            <div class="row marbottom">
                <div class="col-sm-12 col-md-7 col-lg-5 pull-right wow fadeInUp">
                    <h2>Our Services</h2>
                    <p class="pull-right">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus
                        est eget congue. Nam auctor nisi est, nec tempus lacus.</p>
                </div>
            </div>
            <div class="row marbottom wow fadeInUp">
                <div class="col-sm-12 col-md-7 col-lg-7 col-padright-none">
                    <div class="subtitle">
                        <h2 class="titile col-xs-offset-1 col-sm-offset-0 col-md-offset-1 ">CUTTING</h2>
                    </div>
                    <img src="images/cutting.jpg" class="img-responsive" alt="cutting">
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5 col-padleft-none">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>CUT</th>
                            <th>WOMEN</th>
                            <th>MEN</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Stylist</td>
                            <td>$80</td>
                            <td>$70</td>
                        </tr>
                        <tr>
                            <td>Senior Stylist</td>
                            <td>$90</td>
                            <td>$80</td>
                        </tr>
                        <tr>
                            <td>Master Stylist</td>
                            <td>$110</td>
                            <td>$100</td>
                        </tr>
                        <tr>
                            <td>Celebrity Stylist</td>
                            <td>$POA</td>
                            <td>$POA</td>
                        </tr>
                        <tr>
                            <td>Cut</td>
                            <td>$</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Cut</td>
                            <td>$</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Cut</td>
                            <td>$</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Cut</td>
                            <td>$</td>
                            <td>$</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row marbottom wow fadeInUp">
                <div class="col-sm-12 col-md-7 col-lg-7 col-padleft-none displayhide">
                    <div class="subtitle">
                        <h2 class="titile col-xs-offset-2">COLOUR</h2>
                    </div>
                    <div class="subtitle">
                        <h2 class="color">COLOUR</h2>
                    </div>
                    <img src="images/color.jpg" class="img-responsive" alt="Colour"></div>
                <div class="col-sm-12 col-md-5 col-lg-5 col-padright-none">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>COLOUR</th>
                            <th>Jnr</th>
                            <th>Snr</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="bigw">Permanent, Demi Gloss</td>
                            <td class="smallw">from $70</td>
                            <td>from $80</td>
                        </tr>
                        <tr>
                            <td>Colour Correction</td>
                            <td>from $90</td>
                            <td>from $110</td>
                        </tr>
                        <tr>
                            <td>Fashion Foiling</td>
                            <td>from $75</td>
                            <td>from $85</td>
                        </tr>
                        <tr>
                            <td>Tint roots</td>
                            <td>$75</td>
                            <td>$85</td>
                        </tr>
                        <tr>
                            <td>Tint and foils</td>
                            <td>$110</td>
                            <td>$120</td>
                        </tr>
                        <tr>
                            <td>Half Head - Short</td>
                            <td>from $120</td>
                            <td>from $130</td>
                        </tr>
                        <tr>
                            <td>Half Head - Long</td>
                            <td>from $150</td>
                            <td>from $170</td>
                        </tr>
                        <tr>
                            <td>Full Head - Short</td>
                            <td>from $160</td>
                            <td>from $180</td>
                        </tr>
                        <tr>
                            <td>Full Head - Long</td>
                            <td>from $220</td>
                            <td>from $250</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-12 col-md-7 col-lg-7 col-padleft-none displayvisible">
                    <div class="subtitle">
                        <h2 class="titile col-xs-offset-2">COLOUR</h2>
                    </div>
                    <img src="images/color.jpg" class="img-responsive" alt="Colour"></div>
            </div>
            <div class="row wow fadeInUp">
                <div class="col-sm-12 col-md-7 col-lg-7 col-padright-none">
                    <div class="subtitle">
                        <h2 class="titile col-xs-offset-1">STYLE</h2>
                    </div>
                    <img src="images/style.jpg" class="img-responsive" alt="Style"></div>
                <div class="col-sm-12 col-md-5 col-lg-5 col-padleft-none">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>STYLE</th>
                            <th>PRICE</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Blowdrying - Short</td>
                            <td>$60</td>
                        </tr>
                        <tr>
                            <td>Blowdrying - Medium</td>
                            <td>$70</td>
                        </tr>
                        <tr>
                            <td>Blowdrying - Long</td>
                            <td>$80</td>
                        </tr>
                        <tr>
                            <td>Formals</td>
                            <td>$130</td>
                        </tr>
                        <tr>
                            <td>Bride</td>
                            <td>$180</td>
                        </tr>
                        <tr>
                            <td>Style</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Style</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Style</td>
                            <td>$</td>
                        </tr>
                        <tr>
                            <td>Style</td>
                            <td>$</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <section id="appoinment" class="col-padtop wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="appoimentbg">
                        <div class="col-sm-12 col-md-9 col-lg-8">
                            <h2>make an appointment</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                congue. Nam auctor nisi est, nec tempus lacus viverra nec.</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="appfrm">
                            <div id="SuccessMessage"></div>
                            <div id="ErrorMessage"></div>
                            <form name="AppointmentFrm" id="AppointmentFrm" method="post">
                                <div class="col-sm-12 col-md-8 col-lg-7 appfrmleft">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="AppointmentFullName" id="AppointmentFullName"
                                               class="form-control required">
                                    </div>
                                    <div class="form-group mt-right0">
                                        <label>Email</label>
                                        <input type="email" class="form-control required email" name="AppointmentEmail"
                                               id="AppointmentEmail"/>
                                    </div>
                                    <div class="form-group pull-left">
                                        <label>Contact Number</label>
                                        <input type="text" name="AppointmentContactNumber" id="AppointmentContactNumber"
                                               class="form-control required number">
                                    </div>
                                    <div class="form-group mt-right0">
                                        <div class="input-append dateinput">
                                            <label class="control-label">Date</label>
                                            <div class="desktopdate"><input type="text" class="form-control required"
                                                                            name="AppointmentDate" id="datePicker"/>
                                            </div>
                                            <div class="mobiledate"><input type="date" class="form-control required"
                                                                           name="AppointmentMobileDate"/></div>
                                        </div>
                                        <div class="time">
                                            <label>Time</label>
                                            <select name="AppointmentTime" id="AppointmentTime">
                                                <option selected>Select</option>
                                                <option>11:00am</option>
                                                <option>12:00pm</option>
                                                <option>01:00pm</option>
                                                <option>02:00pm</option>
                                                <option>03:00pm</option>
                                                <option>04:00pm</option>
                                                <option>05:00pm</option>
                                                <option>06:00pm</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 col-lg-5 appfrmright">
                                    <div class="form-group textarea">
                                        <label class="control-label">Description</label>
                                        <textarea class="form-control" name="AppointmentMessage" rows="5"
                                                  id="AppointmentMessage"></textarea>
                                    </div>
                                    <div class="submitbtn">
                                        <button type="submit" class="btn btn-default" value="Submit">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="excellence wow fadeInUp">
        <div id="parallax-2" class="parallax fixed fixed-desktop">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-5 col-lg-5 pull-right col-pad5 bg-white">
                        <h2>Expression of Excellence</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue.
                            Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non congue aliquam,
                            mauris massa consequat sem, ut laoreet nisi erat et lectus. Nullam non neque eros.
                            Pellentesque nec vulputate eros. Integer scelerisque lorem id massa accumsan, ut faucibus
                            ante suscipit. Nunc tincidunt et ligula vitae pharetra.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
    <section id="gallery">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Proud portfolio</h2>
                </div>
                <div class="col-lg-12 filter">
                    <ul>
                        <li><a href="#" data-filter="*" class="active">All</a></li>
                        <li><a href="#" data-filter=".Wcut">Cutting</a></li>
                        <li><a href="#" data-filter=".coloring">Coloring</a></li>
                        <li><a href="#" data-filter=".Hspa">Conditioning</a></li>
                        <li><a href="#" data-fancybox-group="manucuring" data-filter=".manucuring">Manicuring</a></li>
                        <li><a href="#" data-filter=".Bspa">Styling</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="portfoliodiv">
                    <div class="col-25 coloring manucuring"><a class="fancybox" href="images/gallery/large/1.jpg"
                                                               data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/1.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Cut</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
                    <div class="col-25 Wcut manucuring"><a class="fancybox" href="images/gallery/large/2.jpg"
                                                           data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/2.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Style</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25 coloring Hspa"><a class="fancybox" href="images/gallery/large/3.jpg"
                                                         data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/3.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Cut</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25 Wcut"><a class="fancybox" href="images/gallery/large/4.jpg"
                                                data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/4.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Style</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25 coloring"><a class="fancybox" href="images/gallery/large/5.jpg"
                                                    data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/5.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Cut</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25 Bspa"><a class="fancybox" href="images/gallery/large/6.jpg"
                                                data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/6.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Style</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25 Bspa"><a class="fancybox" href="images/gallery/large/7.jpg"
                                                data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/7.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Cut</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                    <div class="col-25"><a class="fancybox" href="images/gallery/large/8.jpg"
                                           data-fancybox-group="gallery">
                        <div class="hover">
                            <img src="images/gallery/large/8.jpg" alt="Portfolio"/>
                            <div class="mask-img">
                                <div class="hvrlink">
                                    <h3>Style</h3>
                                    <p>Lorem Ipsum is simply dummy text of the printing industry.</p>
                                </div>
                            </div>
                        </div>
                    </a></div>
                </div>
            </div>
        </div>
    </section>
    <section id="promotions" class="col-padtop wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-gray">
                        <h2 class="text-center">Promotions and Specials</h2>
                        <div class="col-sm-12 col-lg-12">
                            <div id="myCarousel-1" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <div class="img-right"><img src="images/sider-mini/P-slide-1.jpg"
                                                                    alt="First slide" class="img-responsive"></div>
                                        <div class="col-pad4 gbg-white">
                                            <h4>Free hair treatment</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar
                                                luctus est eget congue. Nam auctor nisi est, nec tempus lacus viverra
                                                nec. Nullam cursus, neque non congue aliquam, mauris massa consequat
                                                sem, ut laoreet nisi erat et lectus.</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="img-right"><img src="images/sider-mini/P-slide-2.jpg"
                                                                    alt="First slide" class="img-responsive"></div>
                                        <div class="gbg-white col-pad4">
                                            <h4>Monday Makeup</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar
                                                luctus est eget congue. Nam auctor nisi est, nec tempus lacus viverra
                                                nec. Nullam cursus, neque non congue aliquam, mauris massa consequat
                                                sem, ut laoreet nisi erat et lectus.</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="img-right"><img src="images/sider-mini/P-slide-3.jpg"
                                                                    alt="First slide" class="img-responsive"></div>
                                        <div class="gbg-white col-pad4">
                                            <h4>Happy Hair</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar
                                                luctus est eget congue. Nam auctor nisi est, nec tempus lacus viverra
                                                nec. Nullam cursus, neque non congue aliquam, mauris massa consequat
                                                sem, ut laoreet nisi erat et lectus.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-control"><a class="left" href="#myCarousel-1" role="button"
                                                                 data-slide="prev"></a> <a class="right"
                                                                                           href="#myCarousel-1"
                                                                                           role="button"
                                                                                           data-slide="next"></a></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="video" class="col-padtop wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-8">
                    <div class="responsive-object">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/fLuuCDDMy7M" frameborder="0"
                                allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-md-5 col-lg-4 pull-right">
                    <h2>Showreel</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget congue. Nam
                        auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non congue aliquam, mauris
                        massa consequat sem, ut laoreet nisi erat et lectus. Nullam non neque eros. Pellentesque nec
                        vulputate eros. Integer scelerisque lorem id massa accumsan, ut faucibus ante suscipit. Nunc
                        tincidunt et ligula vitae pharetra. Fusce ut lobortis augue, eget volutpat felis.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonialdiv wow fadeInUp">
        <div class="container">
            <div class="testimonilabg">
                <h2>Testimonials</h2>
                <div id="myCarousel-2" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="img-left"><img src="images/user-icon.jpg" alt="Testimonial"
                                                       class="img-responsive"></div>
                            <div class="col-pad4 tbg-white">
                                <h4>Free hair treatment</h4>
                                <h6>Advertising Model</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non
                                    congue aliquam, mauris massa consequat sem, ut laoreet nisi erat et lectus.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-left"><img src="images/user-icon.jpg" alt="Testimonial"
                                                       class="img-responsive"></div>
                            <div class="col-pad4 tbg-white">
                                <h4>Monday Makeup</h4>
                                <h6>Advertising Model</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non
                                    congue aliquam, mauris massa consequat sem, ut laoreet nisi erat et lectus.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-left"><img src="images/user-icon.jpg" alt="Testimonial"
                                                       class="img-responsive"></div>
                            <div class="col-pad4 tbg-white">
                                <h4>Free hair treatment</h4>
                                <h6>Advertising Model</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non
                                    congue aliquam, mauris massa consequat sem, ut laoreet nisi erat et lectus.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="img-left"><img src="images/user-icon.jpg" alt="Testimonial"
                                                       class="img-responsive"></div>
                            <div class="col-pad4 tbg-white">
                                <h4>Monday Makeup</h4>
                                <h6>Advertising Model</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar luctus est eget
                                    congue. Nam auctor nisi est, nec tempus lacus viverra nec. Nullam cursus, neque non
                                    congue aliquam, mauris massa consequat sem, ut laoreet nisi erat et lectus.</p>
                            </div>
                        </div>

                    </div>
                    <div class="carousel-control"><a class="left" href="#myCarousel-2" role="button"
                                                     data-slide="prev"></a> <a class="right" href="#myCarousel-2"
                                                                               role="button" data-slide="next"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="wow fadeInUp">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Contact</h2>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="contactmap">
                        <div class="mapcont">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d25216.227802888236!2d144.956981!3d-37.812802!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4e793770d3%3A0x9e44d6ad0d76ba7c!2s121+King+St%2C+Melbourne+VIC+3000%2C+Australia!5e0!3m2!1sen!2sus!4v1429013152809"
                                    style="border:0"></iframe>
                        </div>
                        <div class="social">
                            <p>121 King Street, Melbourne Victoria 3000 Australia</p>
                            <span>Phone - +61 0 0000 0000s </span> <span>Email - <a href="mailto:info@websitec.com">info@blackair.com</a></span>
                            <div class="social-icon"><a href="#" class="facebook"></a> <a href="#" class="twitter"></a>
                                <a href="#" class="google"></a> <a href="#" class="youtube"></a></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 pull-right">
                    <div id="ContactSuccessMessage"></div>
                    <div id="ContactErrorMessage"></div>
                    <form name="ContactForm" id="ContactForm" method="post">
                        <div class="form-group pull-left">
                            <input type="text" class="form-control required" name="ContactFullName" id="ContactFullName"
                                   placeholder="Name">
                        </div>
                        <div class="form-group pull-left marright0">
                            <input type="email" class="form-control required email" name="ContactEmail"
                                   id="ContactEmail" placeholder="Email Id">
                        </div>
                        <div class="form-group pull-left">
                            <input type="text" class="form-control required number" name="ContactNumber"
                                   id="ContactNumber" placeholder="Contact Number">
                        </div>
                        <div class="form-group pull-left marright0">
                            <input type="text" class="form-control required" name="ContactCompanyName"
                                   id="ContactCompanyName" placeholder="Company Name">
                        </div>
                        <div class="textarea pull-left">
                            <textarea placeholder="Description" name="ContactDescription" id="ContactDescription"
                                      class="form-control"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default" value="Submit">SUBMIT</button>
                    </form>
                    <div class="coypright">
                        <p>&copy; Website 2015</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <a href="#" class="scrollup">Top</a>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Recipient:</label>
                        <input type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" id="message-text"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.1.11.2.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/function.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/parallax.js"></script>
<script src="js/scorll.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/slick.js"></script>
<script src="js/menu.js"></script>
<script src="js/ios-timer.js"></script>
<script src="js/jquery.fencybox.js"></script>
<script src="js/jquery.portfolio.js"></script>
<script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery.validate.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="js/revoluation/jquery.themepunch.tools.min.js"></script>
<script src="js/revoluation/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
(Load Extensions only on Local File Systems !
The following part can be removed on Server for On Demand Loading) -->
<script src="js/revoluation/revolution.extension.layeranimation.min.js"></script>
<script src="js/revoluation/revolution.extension.migration.min.js"></script>
<script src="js/revoluation/revolution.extension.navigation.min.js"></script>
<script src="js/revoluation/revolution.extension.parallax.min.js"></script>
<script src="js/revoluation/revolution.extension.slideanims.min.js"></script>
<script src="js/revoluation/revoluationfunction.js"></script>

<%--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>--%>
<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>--%>
<%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--%>

<%--Popup login--%>
<%--<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>--%>
<%--<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>--%>

</body>

<!-- Mirrored from lithemes.com/blackair/v2/white/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Apr 2018 20:14:12 GMT -->
</html>

