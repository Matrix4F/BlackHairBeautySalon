<%--
  Created by IntelliJ IDEA.
  User: Nuwan
  Date: 8/21/2018
  Time: 7:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
<script type="text/javascript" src="${path}/js/angularCode.js"></script>

<body>

<div class="page-header" >
    <h1 class="page-title">Add Appointment</h1>
    <ol class="breadcrumb">
        <li><a href="">Dashboard</a></li>
        <li class="active">Add Appointment</li>

    </ol>

    <div class="page-header-actions">

    </div>
</div>

    <div class="page-content" ng-app="app">
        <div class="panel">

            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Add a new appointment</h3>
            </header>
            <div ng-controller="postAppoinmentCtr as ctrl" class="panel-body">

                <form ng-submit="submitForm()" id="form1">



                    <div class="form-group">
                        <label for="program_name">Appointment Type</label>
                        <select  ng-model="type" class="form-control" data-plugin="selectpicker">
                            <option>Hair Cut</option>
                            <option>Hair Trim</option>
                            <option>Bread Save</option>
                        </select>
                    </div>


                    <%--WorkedOne--%>
                    <%--<input id="date-birth" class="form-control" type="date" ng-model="date">--%>

                    <div class="form-group">
                        <label for="starting_date">Select Date and Time</label>
                        <div class="input-date">
                            <div class="input-group">
                                <span class="input-group-addon">
                                  <i class="icon wb-calendar" aria-hidden="true"></i>
                                </span>

                                    <input id="dateValue" name="dateXX" ng-model="dateX" type="text" class="form-control" data-plugin="datepicker"/>

                            </div>
                            <br/>
                            <div class="input-group" id="disableTimeRangesExample">
                                <span class="input-group-addon">
                                  <i class="icon wb-time" aria-hidden="true"></i>
                                </span>
                                <%--<input name="time" ng-model="time" type="time" class="form-control" autocomplete="off"/>--%>
                                <input id="timeValue" name="timeXX" ng-model="timeX" type="text" class="form-control"  data-plugin="timepicker" autocomplete="off"/>
                            </div>
                        </div>


                        <%--
                        <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="icon wb-calendar" aria-hidden="true"></i>
                                                </span>
                            <input type="text" class="form-control" data-format="yyyy-mm-dd" data-plugin="datepicker" name="starting_date" id="fld_dob" placeholder="YYYY-MM-DD">
                        </div>--%>
                    </div>

                    <div class="form-group">
                        <label for="num_years">Description</label>
                        <textarea ng-model="description" row="10" class="form-control" id="description" name="description" style="resize: none;"></textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <button type="submit" class="col-md-2 col-sm-offset-10 btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </div>

    </div>
<!-- End Page -->
<!-- Footer -->
<%@include file="includes/includejs.jsp" %>

<%--<script>
    $('#disableTimeRangesExample').timepicker({
        'disableTimeRanges': [
            ['12am', '7am'],
            ['10pm', '11:30pm']
        ]
    });
</script>--%>
<script>
    $('#check-out').datepicker();
</script>

<script>

    function assignConfirmationToAccept() {
        if (confirm("Application will be Accepted and can't undo this operation!. Do you want to Approve this Record...?")) {

            return true;
        } else
        {
            return false;
        }

    }
</script>

<!--Confirm box create area for reject button-->
<script>

    function assignConfirmationToReject() {
        if (confirm("Application will be Rejected and can't undo this operation!. Do you want to Reject this Record ?")) {
            return true;
        } else
        {
            return false;
        }

    }
</script>

<script>
    $(document).ready(function () {
        $('.acrebtn').delegate('.accept', 'click', function (e) {

            var confirm = assignConfirmationToAccept();
            if (confirm === true) {
                var Id = jQuery(this).find('input').val(); //getting input button value for accept button
                var decision = "approved";

                $.ajax({url: 'ApplicationServlet?id=' + Id + '&decision=' + decision, success: function (data) {

                        $("?accept").html(data);
                        window.location = "applications.jsp";
//                        window.location.reload(true);

                    }});
                // e.preventDefault();
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('.acrebtn').delegate('.reject', 'click', function (e) {

            var confirm = assignConfirmationToReject();
            if (confirm === true) {
                var Id = jQuery(this).find('input').val(); //getting input button value for reject button
                var decision = "rejected";

                $.ajax({url: 'ApplicationServlet?id=' + Id + '&decision=' + decision, success: function (data) {

                        $("?reject").html(data);
                        window.location = "applications.jsp";
//                        window.location.reload(true);

                    }});
                // e.preventDefault();
            }
        });
    });
</script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;

        $(document).ready(function ($) {
            Site.run();
        });

        // Individual column searching
        // ---------------------------
        (function () {
            $(document).ready(function () {
                var defaults = $.components.getDefaults("dataTable");

                var options = $.extend(true, {}, defaults, {
                    initComplete: function () {
                        this.api().columns().every(function () {
                            var column = this;
                            var select = $(
                                '<select class="form-control width-full" data-plugin="selectpicker"><option value=""></option></select>'
                            )
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                    );

                                    column
                                        .search(val ? '^' + val + '$' : '',
                                            true, false)
                                        .draw();
                                });

                            column.data().unique().sort().each(function (
                                d, j) {
                                select.append('<option value="' + d +
                                    '">' + d + '</option>');
                            });
                        });
                    }
                });

                $('#exampleTableSearch').DataTable(options);
            });
        })();

        // Table Tools
        // -----------
        (function () {
            $(document).ready(function () {
                var defaults = $.components.getDefaults("dataTable");

                var options = $.extend(true, {}, defaults, {
                    "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': [-1]
                    }],
                    "iDisplayLength": 5,
                    "aLengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
                    "oTableTools": {
                        "sSwfPath": "assets/vendor/datatables-tabletools/swf/copy_csv_xls_pdf.swf"
                    }
                });

                $('#exampleTableTools').dataTable(options);
            });

        })();

    })(document, window, jQuery);
</script>

</body>

</html>


