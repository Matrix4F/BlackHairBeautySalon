<%
    session.setAttribute( "theName", "name" );
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var ="path" value="${pageContext.request.contextPath}"></c:set>

<!DOCTYPE html>

<!--Administrator side navigation table-->
<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu">

                    <%--<c:if test="${pageContext.request.userPrincipal.name == 'admin'}">
                        <li class="site-menu-item">
                            <a class="animsition-link" href="main" data-slug="dashboard-v1">
                                <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                                <span class="site-menu-title">User Details</span>
                            </a>
                        </li>

                        <li class="site-menu-item">
                            <a class="animsition-link" href="main" data-slug="layout-headers">
                                <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                <span class="site-menu-title">Appointment</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${pageContext.request.userPrincipal.name != 'admin'}">
                        <li class="site-menu-item">
                            <a class="animsition-link" href="main" data-slug="layout-headers">
                                <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                <span class="site-menu-title">Appointment</span>
                            </a>
                        </li>
                    </c:if>--%>
                        <%--<c:if test="${not empty pageContext.request.userPrincipal}">

                            <c:if test="${pageContext.request.isUserInRole('ADMIN')}">

                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="dashboard-v1">
                                        <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                                        <span class="site-menu-title">User Details</span>
                                    </a>
                                </li>

                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="layout-headers">
                                        <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                        <span class="site-menu-title">Appointment</span>
                                    </a>
                                </li>

                            </c:if>
                            <c:if test="${pageContext.request.isUserInRole('user')}">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="layout-headers">
                                        <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                        <span class="site-menu-title">Appointment</span>
                                    </a>
                                </li>
                            </c:if>

                        </c:if>--%>
                    <c:if test="${not empty pageContext.request.userPrincipal}">
                        <c:choose>
                            <c:when test="${pageContext.request.isUserInRole('ADMIN')}">
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="dashboard-v1">
                                        <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                                        <span class="site-menu-title">User Details</span>
                                    </a>
                                </li>

                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="layout-headers">
                                        <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                        <span class="site-menu-title">Appointment</span>
                                    </a>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="site-menu-item">
                                    <a class="animsition-link" href="main" data-slug="layout-headers">
                                        <i class="site-menu-icon wb-layout" aria-hidden="true"></i>
                                        <span class="site-menu-title">Appointment</span>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:if>

                </ul>

            </div>
        </div>
    </div>

    <div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
            <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a>
        <a href="LoginServlet" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon wb-power" aria-hidden="true"></span>
        </a>
    </div>
</div>
<div class="site-gridmenu">
    <ul>

        <li>
            <a href="../CDCE/calendar.jsp">
                <i class="icon wb-calendar"></i>
                <span>Calendar</span>
            </a>
        </li>

        <li>
            <a href="../CDCE/dashboard.jsp">
                <i class="icon wb-dashboard"></i>
                <span>Dashboard</span>
            </a>
        </li>
    </ul>
</div>

