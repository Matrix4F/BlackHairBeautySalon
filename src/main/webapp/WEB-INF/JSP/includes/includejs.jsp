<%--
    Document   : footer_admin
    Created on : Oct 5, 2016, 2:18:50 PM
    Author     : nuwan
--%>
<!DOCTYPE html>
<!--Administrator footer-->
<!--End footer-->

<script src="assets/vendor/jquery/jquery.js"></script>
<script src="assets/vendor/bootstrap/bootstrap.js"></script>
<script src="assets/vendor/animsition/jquery.animsition.js"></script>
<script src="assets/vendor/asscroll/jquery-asScroll.js"></script>
<script src="assets/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="assets/vendor/asscrollable/jquery.asScrollable.all.js"></script>
<script src="assets/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
<script src="assets/vendor/bootstrap-select/bootstrap-select.js"></script>

<!-- Plugins -->
<script src="assets/vendor/switchery/switchery.min.js"></script>
<script src="assets/vendor/intro-js/intro.js"></script>
<script src="assets/vendor/screenfull/screenfull.js"></script>
<script src="assets/vendor/slidepanel/jquery-slidePanel.js"></script>

<script src="assets/vendor/skycons/skycons.js"></script>
<script src="assets/vendor/chartist-js/chartist.min.js"></script>
<script src="assets/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
<script src="assets/vendor/jvectormap/jquery-jvectormap.min.js"></script>
<script src="assets/vendor/jvectormap/maps/jquery-jvectormap-ca-lcc-en.js"></script>
<script src="assets/vendor/matchheight/jquery.matchHeight-min.js"></script>

<script src="assets/vendor/moment/moment.min.js"></script>
<script src="assets/vendor/fullcalendar/fullcalendar.js"></script>
<script src="assets/vendor/jquery-selective/jquery-selective.min.js"></script>
<script src="assets/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

<script src="assets/vendor/select2/select2.min.js"></script>
<script src="assets/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
<script src="assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="assets/vendor/bootstrap-select/bootstrap-select.js"></script>
<script src="assets/vendor/icheck/icheck.min.js"></script>
<script src="assets/vendor/switchery/switchery.min.js"></script>
<script src="assets/vendor/asrange/jquery-asRange.min.js"></script>
<script src="assets/vendor/asspinner/jquery-asSpinner.min.js"></script>
<script src="assets/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
<script src="assets/vendor/ascolor/jquery-asColor.min.js"></script>
<script src="assets/vendor/asgradient/jquery-asGradient.min.js"></script>
<script src="assets/vendor/ascolorpicker/jquery-asColorPicker.min.js"></script>
<script src="assets/vendor/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="assets/vendor/jquery-knob/jquery.knob.js"></script>
<script src="assets/vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="assets/vendor/card/jquery.card.js"></script>
<script src="assets/vendor/jquery-labelauty/jquery-labelauty.js"></script>
<script src="assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="assets/vendor/jt-timepicker/jquery.timepicker.min.js"></script>
<script src="assets/vendor/datepair-js/datepair.min.js"></script>
<script src="assets/vendor/datepair-js/jquery.datepair.min.js"></script>
<script src="assets/vendor/jquery-strength/jquery-strength.min.js"></script>
<script src="assets/vendor/multi-select/jquery.multi-select.js"></script>
<script src="assets/vendor/typeahead-js/bloodhound.min.js"></script>
<script src="assets/vendor/typeahead-js/typeahead.jquery.min.js"></script>
<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>



<script src="assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="assets/vendor/jt-timepicker/jquery.timepicker.min.js"></script>
<script src="assets/vendor/datepair-js/datepair.min.js"></script>
<script src="assets/vendor/datepair-js/jquery.datepair.min.js"></script>
<script src="assets/vendor/jquery-strength/jquery-strength.min.js"></script>
<script src="assets/vendor/multi-select/jquery.multi-select.js"></script>
<script src="assets/vendor/formvalidation/formValidation.js"></script>
<script src="assets/vendor/formvalidation/framework/bootstrap.js"></script>
<script src="assets/vendor/matchheight/jquery.matchHeight-min.js"></script>
<script src="assets/vendor/jquery-wizard/jquery-wizard.js"></script>

<!--Table plugins-->
<script src="assets/vendor/datatables/jquery.dataTables.js"></script>
<script src="assets/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
<script src="assets/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="assets/vendor/datatables-responsive/dataTables.responsive.js"></script>
<script src="assets/vendor/datatables-tabletools/dataTables.tableTools.js"></script>

<!-- Plugins For alert -->
<script src="assets/vendor/bootbox/bootbox.js"></script>
<script src="assets/vendor/bootstrap-sweetalert/sweet-alert.js"></script>
<script src="assets/vendor/toastr/toastr.js"></script>
<script src="assets/vendor/alertify-js/alertify.js"></script>

<!-- Scripts -->
<script src="assets/js/core.js"></script>
<script src="assets/js/site.js"></script>

<script src="assets/js/sections/menu.js"></script>
<script src="assets/js/sections/menubar.js"></script>
<script src="assets/js/sections/sidebar.js"></script>

<script src="assets/js/configs/config-colors.js"></script>
<script src="assets/js/configs/config-tour.js"></script>

<script src="assets/js/components/asscrollable.js"></script>
<script src="assets/js/components/animsition.js"></script>
<script src="assets/js/components/slidepanel.js"></script>
<script src="assets/js/components/switchery.js"></script>
<script src="assets/js/components/select2.js"></script>
<script src="assets/js/components/bootstrap-tokenfield.js"></script>
<script src="assets/js/components/bootstrap-tagsinput.js"></script>

<script src="assets/js/components/matchheight.js"></script>
<script src="assets/js/components/jvectormap.js"></script>
<script src="assets/js/components/datatables.js"></script>
<script src="assets/js/components/icheck.js"></script>

<script src="assets/js/components/bootstrap-select.js"></script>
<script src="assets/js/components/bootstrap-datepicker.js"></script>
<script src="assets/js/components/jt-timepicker.js"></script>
<script src="assets/js/components/datepair-js.js"></script>
<script src="assets/js/components/jquery-strength.js"></script>
<script src="assets/js/components/multi-select.js"></script>
<script src="assets/js/components/jquery-wizard.js"></script>
<script src="assets/js/components/matchheight.js"></script>
<script src="assets/js/form_validation.js"></script>

<script src="assets/js/components/asrange.js"></script>
<script src="assets/js/components/asspinner.js"></script>
<script src="assets/js/components/clockpicker.js"></script>
<script src="assets/js/components/ascolorpicker.js"></script>
<script src="assets/js/components/bootstrap-maxlength.js"></script>
<script src="assets/js/components/jquery-knob.js"></script>
<script src="assets/js/components/bootstrap-touchspin.js"></script>
<script src="assets/js/components/card.js"></script>
<script src="assets/js/components/jquery-labelauty.js"></script>
<script src="assets/js/components/bootstrap-datepicker.js"></script>
<script src="assets/js/components/jt-timepicker.js"></script>
<script src="assets/js/components/datepair-js.js"></script>
<script src="assets/js/components/jquery-strength.js"></script>
<script src="assets/js/components/multi-select.js"></script>
<script src="assets/js/components/jquery-placeholder.js"></script>

<!--For calendar-->
<script src="assets/js/components/bootstrap-touchspin.js"></script>
<script src="assets/js/components/bootstrap-datepicker.js"></script>
<script src="assets/js/apps/app.js"></script>
<script src="assets/js/apps/calendar.js"></script>

<!--For Editor-->
<script src="assets/vendor/bootstrap-markdown/bootstrap-markdown.js"></script>
<script src="assets/vendor/marked/marked.js"></script>
<script src="assets/vendor/to-markdown/to-markdown.js"></script>


<!--Application Multiple Add/Remove table-->
<%--<script>

    ///////////////////////////// Multiple Professional Qualifications ///////////////////////////////////////////

    // Show Add Multiple Value option when user checked 'Yes'
    $('#professional_qualifications_yes').change(function () {
        if ($(this).is(":checked")) {
            document.getElementById("professional_qualifications_multiple").removeAttribute("hidden");

        }
    });

    // Hide Add Multiple Value option when user checked 'No'
    $('#professional_qualifications_no').change(function () {
        if ($(this).is(":checked")) {
            document.getElementById("professional_qualifications_multiple").setAttribute("hidden", "hidden");
        }
    });

    var countProfessionalQualifications = parseInt('${empty professionalQualifications ? 1 : professionalQualifications.size()}');

    var iCntQualification = countProfessionalQualifications;
    var row_count_professional_qualifications = countProfessionalQualifications;

    $("#row_count_professional_qualifications").val(countProfessionalQualifications);

    $('#btnAddQualification').on("click", function () {

        iCntQualification = iCntQualification + 1;
        row_count_professional_qualifications += 1;
        $("#row_count_professional_qualifications").val(row_count_professional_qualifications);

        $("#professional_qualifications_multiple").append('<div class="row" id="row_professional_qualifications_' + iCntQualification + '"><br>\n\
        <div class="col-sm-2"><input type="text" class="form-control" id="pq_year_'+ iCntQualification +'" name="year_' + iCntQualification + '" placeholder="Year" autocomplete="off"/>\n\
        </div>\n\
        <div class="col-sm-3"><input type="text" class="form-control" id="pq_course_'+ iCntQualification +'" name="course_' + iCntQualification + '" placeholder="Course" autocomplete="off"/>\n\
        </div>\n\
        <div class="col-sm-3"><input type="text" class="form-control" id="pq_institute_'+ iCntQualification +'" name="institute_' + iCntQualification + '" placeholder="Institute" autocomplete="off"/>\n\
        </div>\n\
        <div class="col-sm-2"><input type="text" class="form-control" id="pq_result_'+ iCntQualification +'" name="result_' + iCntQualification + '" placeholder="Result" autocomplete="off"/></div>\n\
        </div>');
    });

    $('#btnRemoveQualification').click(function () {   // REMOVE ELEMENTS ONE PER CLICK.
        if (iCntQualification !== 1) {
            $('#row_professional_qualifications_' + iCntQualification).remove();
            iCntQualification = iCntQualification - 1;
            row_count_professional_qualifications -= 1;
            $("#row_count_professional_qualifications").val(row_count_professional_qualifications);
        }
    });

    ///////////////////////////// Multiple Professional Qualifications End ///////////////////////////////////////////

    ///////////////////////////// Multiple Working Experience ///////////////////////////////////////////

    // Show Add Multiple Value option when user checked 'Yes'
    $('#fld_working_experience_yes').change(function () {
        if ($(this).is(":checked")) {
            document.getElementById("working_experience_multiple").removeAttribute("hidden");

        }
    });

    // Hide Add Multiple Value option when user checked 'No'
    $('#fld_working_experience_no').change(function () {
        if ($(this).is(":checked")) {
            document.getElementById("working_experience_multiple").setAttribute("hidden", "hidden");
        }
    });

    var countWorkingExperience = parseInt('${empty workingExperience ? 1 : workingExperience.size()}');

    var iCntExperience = countWorkingExperience;
    var row_count_working_experience = countWorkingExperience;

    $("#row_count_working_experience").val(countWorkingExperience);

    $('#btnAddExperience').on("click", function () {

        iCntExperience = iCntExperience + 1;
        row_count_working_experience += 1;
        $("#row_count_working_experience").val(row_count_working_experience);

        $("#working_experience_multiple").append('<div class="row" id="row_working_experience_' + iCntExperience + '"><br>\n\
        <div class="col-sm-5"><div class="col-sm-1"><label class="control-label">From</label></div>\n\
        <div class="col-sm-5"><div class="input-group"><span class="input-group-addon"><i class="icon wb-calendar" aria-hidden="true"></i></span>\n\
        <input type="text" class="form-control" id="we_time_period_begin_'+ iCntExperience +'" name="time_period_begin_' + iCntExperience + '" placeholder="YYYY-MM-DD" data-plugin="datepicker"/>\n\
        </div></div>\n\
        <div class="col-sm-1"><label class="control-label">to</label></div>\n\
        <div class="col-sm-5"><div class="input-group"><span class="input-group-addon"><i class="icon wb-calendar" aria-hidden="true"></i></span>\n\
        <input type="text" class="form-control" id="we_time_period_end_'+ iCntExperience +'" name="time_period_end_' + iCntExperience + '" placeholder="YYYY-MM-DD" data-plugin="datepicker"/>\n\
        </div></div></div>\n\
        <div class="col-sm-3"><input type="text" class="form-control" id="we_institute_'+ iCntExperience +'" name="institute_' + iCntExperience + '" placeholder="Institute" autocomplete="off"/></div>\n\
        <div class="col-sm-2"><input type="text" class="form-control" id="we_position_'+ iCntExperience +'" name="position_' + iCntExperience + '" placeholder="Position" autocomplete="off"/></div>\n\
        </div>');
        $('#we_time_period_begin_' + iCntExperience).datepicker();
        $('#we_time_period_end_' + iCntExperience).datepicker();
    });

    $('#btnRemoveExperience').click(function () {   // REMOVE ELEMENTS ONE PER CLICK.
        if (iCntExperience !== 1) {
            $('#row_working_experience_' + iCntExperience).remove();
            iCntExperience = iCntExperience - 1;
            row_count_working_experience -= 1;
            $("#row_count_working_experience").val(row_count_working_experience);
        }
    });

    ///////////////////////////// Multiple Working Experience End ///////////////////////////////////////////

    //    $.fn.datepicker.defaults.format = 'yy/mm/dd';
    //    $.fn.datepicker.defaults.autoclose = true;
</script>--%>
