<!DOCTYPE html>

<%@include file="includes/header_admin.jsp"%>
<%@include file="includes/side_navibar.jsp" %>

<!-- Page -->

<div class="page animsition">

    <%--<div class="page-header">
        <h1 class="page-title">Appointment</h1>
        <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li class="active">Appointment</li>

        </ol>

        <div class="page-header-actions">

        </div>
    </div>--%>
    <%--<div class="page-content">
        <div class="panel">
            <header class="panel-heading">
                <h4 class="panel-title">Appointment &nbsp;<span class="tooltip-dark" data-toggle="tooltip" data-original-title="Help tips" data-trigger="focus / hover"><button type="button" class="btn-info popover-info btn-sm btn-icon btn-inverse btn-info btn-round" data-content="Can accept or reject any applications for selection test in this table. And can search any content of the table. And also can view fully application through the id." data-toggle="popover" data-original-title="About this table..." data-trigger="click"><i class="grey-500 icon wb-info"></i></button></span></h4>
            </header>
            <div style="text-align: center">

            </div>
            <div class="panel-body">

                <table class="table table-hover dataTable table-striped width-full" id="exampleTableSearch">
                    <thead>
                    <tr>
                        <th>Appointment No</th>
                        <th>Name</th>
                        <th>Mobile Number</th>
                        <th>Added Date</th>
                        <th>Accept/Reject Date</th>
                        <th>Status</th>
                        <th>Edit/View</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>

                    <td>a</td>
                    <td>a</td>
                    <td>b</td>
                    <td>c</td>
                    <td>d</td>
                    <td><span class="label label-success">Approved</span></td>
                    <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span>Edit</button></p></td>
                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span>Delete</button></p></td>

                    </tr>


                    </tbody>
                </table>

            </div>
        </div>
    </div>--%>

    <div>
        <%@include file="appoinment.jsp" %>
    </div>

</div>
<%@include file="includes/footer_admin.jsp" %>
</body>

</html>

