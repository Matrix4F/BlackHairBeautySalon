package com.example.demo.service.impl;

import com.example.demo.model.Appoinment;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.AppoinmentRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepo;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserRepo userRepo;
    private AppoinmentRepository appRepo;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,RoleRepository roleRepository,UserRepo userRepo,AppoinmentRepository appRepo){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRepo = userRepo;
        this.appRepo = appRepo;
    }

    @Override
    public List<User> userList() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findUser(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public String addUser(User user) {

        //user.setRole(roleRepository.findById(new Long(1)));
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        Optional<Role> role = roleRepository.findById(new Long(2));
        user.setRole(role.get());

        userRepo.save(user);


        /*try {
            *//*if(user.getId() == null){
                message = "Added";
            }
            else{
                message ="Updated";
            }*//*
            //user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            //userRepository.save(user).getUserName();
            //Optional<Role> role = roleRepository.findById(user.getRoleId());
            //user.setRole(role.get());
            //jsonObject.put("title" , message+" Confirmation");
            //jsonObject.put("message",userRepository.save(user).getUserName()+" " +message+" successfully.");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        return "Hi";
    }

    @Override
    public String addAppoinment(Appoinment appoinment) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        appoinment.setUser(findByUserName(name));
        appRepo.save(appoinment);
        return "Appoinment added succsessfuly";
    }

    @Override
    public String deleteUser(Long id) {
        JSONObject jsonObject = new JSONObject();
        try{
            userRepository.deleteById(id);
            jsonObject.put("message","User delete successfully");
        }
        catch (JSONException e) {
             e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public List<Role> roleList() {
        return roleRepository.findAll();
    }

    @Override
    public User findByUserName(String userName) {
        User user = userRepository.findByUserName(userName);
        return user;
    }
}
