package com.example.demo.service.impl;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //BCryptPasswordEncoder encoder = passwordEncoder();
        User user = userRepository.findByUserName(username);
        if(user == null){
            throw new UsernameNotFoundException("User name" + username +" not found");
        }
        //return new org.springframework.security.core.userdetails.User(user.getUserName(),encoder.encode(user.getPassword()),getGrantedAutherities(user));
        return new org.springframework.security.core.userdetails.User(user.getUserName(),user.getPassword(),getGrantedAutherities(user));
    }

    /*@Bean
    public BCryptPasswordEncoder passwordEncoder() {

        return new BCryptPasswordEncoder();
    }*/

    private Collection<GrantedAuthority> getGrantedAutherities(User user){

        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if(user.getRole().getName().equals("admin")){
            grantedAuthorities.add(new SimpleGrantedAuthority(("ROLE_ADMIN")));
        }
        grantedAuthorities.add(new SimpleGrantedAuthority(("ROLE_USER")));

        return grantedAuthorities;
    }
}