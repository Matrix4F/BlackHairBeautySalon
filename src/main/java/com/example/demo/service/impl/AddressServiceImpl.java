package com.example.demo.service.impl;

import com.example.demo.model.Address;
import com.example.demo.model.User;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.AddressService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {

    private AddressRepository addressRepository;
    private UserRepository userRepository;


    @Autowired
    public AddressServiceImpl(AddressRepository addressRepository, UserRepository userRepository) {
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;

    }

    @Override
    public List<Address> addressList() {
        return addressRepository.findAll();
    }

    @Override
    public Optional<Address> findAddress(Long id) {
        return addressRepository.findById(id);
    }

    @Override
    public String addAddress(Address address) {
        //https://stackoverflow.com/questions/49316751/spring-data-jpa-findone-change-to-optional-how-to-use-this
        String message ="";
        JSONObject jsonObject = new JSONObject();
        try {

            if(address.getId() == null){
                message = "Added";
            }
            else{
                message ="Updated";
            }
            Optional<User> dbUser = userRepository.findById(address.getUserId());
            address.setUser(dbUser.get());
            addressRepository.save(address);
            jsonObject.put("title" , message+" Confirmation");
            jsonObject.put("message","Address for user "+dbUser.get().getUserName()+" "+message+ " Successfully");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
        /*if(dbUser.isPresent()){
            User user = dbUser.get();
            address.setUser(user);
            addressRepository.save(address);
            message = "Address added successfuly";
        }
        else {
            message = "User not valid";
        }
        return message;*/
    }

    @Override
    public String deleteAddress(Long id) {
        JSONObject jsonObject = new JSONObject();
        try {
            addressRepository.deleteById(id);
            jsonObject.put("message","Address Deleted Successfully");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
