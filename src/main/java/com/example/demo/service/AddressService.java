package com.example.demo.service;

import com.example.demo.model.Address;

import java.util.List;
import java.util.Optional;

public interface AddressService {

    List<Address> addressList();

    Optional<Address> findAddress(Long id);

    String addAddress(Address address);

    String deleteAddress(Long id);
}
