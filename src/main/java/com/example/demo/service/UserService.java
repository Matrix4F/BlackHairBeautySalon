package com.example.demo.service;

import com.example.demo.model.Appoinment;
import com.example.demo.model.Role;
import com.example.demo.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> userList();

    //Optional<User> findUser(Long id);
    Optional<User> findUser(Long id);

    String addUser(User user);

    String addAppoinment(Appoinment appoinment);

    String deleteUser(Long id);

    List<Role> roleList();

    User findByUserName(String userName);

}
