package com.example.demo.controller;

import com.example.demo.Response;
import com.example.demo.model.Appoinment;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {

    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public MainController(UserService userService,UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @RequestMapping("/beautiworld")
    public String home(){
        return "index";
    }

    @RequestMapping("/usersdetails")
    public String usersDet(){
        return "userdetails";
    }

    @RequestMapping("/regForm")
    public String regPage2(){
        return "reg";
    }

    @RequestMapping("/appoinment")
    public String welcome(){
        return "appoinment";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/main")
    public String mainPage(){
        return "main";
    }




    /*@RequestMapping(value = "/addAppoinment/addAppoinment")
    public void postAppoinment(@RequestBody Appoinment appoinment){
        userService.addAppoinment(appoinment);
    }*/

    /* @RequestMapping(value = "/regForm/postcustomer", method = RequestMethod.POST)
    public void postCustomer(@RequestBody User user) {
        userService.addUser(user);

    }*/
    /*@RequestMapping(value = "/appoinment/allCustomerDetails")
    public List<User> allCustomerDetails() {
        *//*If doesnt work then directly call userReopository.findall() method*//*
        *//*Iterable<User> customers = userService.userList();
        return new Response("Done",customers);*//*

        List<User> customers = userRepository.findAll();
        System.out.println(customers.get(0).getUserName());
        customers.get(1).getUserName();
        //return new Response("Done",customers);
        return customers;
    }*/

    /*@RequestMapping(value = "/registration",method = RequestMethod.POST)
    public  String addUser(@ModelAttribute("userSubmit") User user, BindingResult result, ModelMap model) {
        User usr = user;
        String name = usr.getUserName();
        userService.addUser(user);
        return "cc";
    }
    @RequestMapping(value="/regform",method=RequestMethod.POST)
    public String saveMessage(@ModelAttribute("userSubmit") User user, BindingResult result, ModelMap model) {
        String s = user.getUserName();
        return "homepage";
    }
    @GetMapping("/form")
    public String userForm(Model model) {
        model.addAttribute("userForm",new User());
        return "/form";
    }
    @RequestMapping(value = "/addEmployee", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("userForm")User user,
                         BindingResult result, ModelMap model) {

        userService.addUser(user);
        return "Success";

    }*/
}
