package com.example.demo.controller;

import com.example.demo.model.Appoinment;
import com.example.demo.model.User;
import com.example.demo.repository.AppoinmentRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.math.BigInteger;
import java.util.List;

@RestController
public class IndexController implements ErrorController {

    private UserRepository userRepository;
    private UserService userService;
    private AppoinmentRepository appoinmentRepository;

    @Autowired
    public IndexController(UserService userService,UserRepository userRepository,AppoinmentRepository appoinmentRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.appoinmentRepository = appoinmentRepository;
    }
    private static final String PATH ="/error";

    @Override
    public String getErrorPath() {
        return PATH;
    }

    @RequestMapping(PATH)
    public String error(){
        return "No Mapping Available";
    }

    @RequestMapping(value = "/main/allCustomerDetails")
    public List<User> allCustomerDetails() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        List<User> customers = userRepository.findAll();
        return customers;
    }

    @RequestMapping(value = "/main/getAllAppoinmentDetails")
    public List<Appoinment> getAllAppoinmentDetails(){
        List<Appoinment> appoinment = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); //get logged in username
        User user = userRepository.findByUserName(name);
        Long id = user.getId();
        BigInteger bi;
        bi = BigInteger.valueOf(id);
        try {
            appoinment = appoinmentRepository.findAppoinmentByUserId(id);
        }catch(Exception e){
            e.printStackTrace();
        }
        return appoinment;
    }

    @RequestMapping(value = "/regForm/postcustomer", method = RequestMethod.POST)
    public void postCustomer(@RequestBody User user) {
        userService.addUser(user);

    }

    @RequestMapping(value = "/main/addAppoinment")
    public void postAppoinment(@RequestBody Appoinment appoinment){
        userService.addAppoinment(appoinment);
    }
}
