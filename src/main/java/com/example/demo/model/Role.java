package com.example.demo.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Role extends AbstractPersistable<Long> {

    private String name;
    private transient Long id;

    @OneToMany(targetEntity = User.class, mappedBy = "role",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<User> user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
