package com.example.demo.repository;

import com.example.demo.model.Appoinment;
import com.example.demo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface AppoinmentRepository extends JpaRepository<Appoinment,Long> {

    @Query("from Appoinment  where user_id = :id")
    List<Appoinment> findAppoinmentByUserId(@Param("id") Long id);

}
