$(function(){
    $("#userList").click(function () {
        fetchList("user");
    });

    $("#addressList").click(function () {
        
        fetchList("address");
    });
});

function modifyData(url){
    $.ajax({
        type : "GET",
        url  : url,
        success : function (data) {
            $(".inner-jsp").html(data);
        }
    });
}

function editForm(type,id) {
    modifyData("/springboot/" +type+ "/edit/"+id);
}

function addForm() {
    modifyData("/beautysalon/regForm");
}

function fetchList(type) {
    modifyData("/springboot/" +type+ "/list");
}

function deleteData(type,id){
    if(confirm("Are you sure you want to delete this ?")){
        $.ajax({
            type : "GET",
            url  : "/springboot/" +type+ "/delete/"+id,
            success : function (data) {
                fetchList(type);
                //alert(data);

            }
        });
    }
    else{
        return false;
    }
}
/*
function deleteData(type,id){
    toastr.warning(
        "<div>Are you sure you want to delete this ?</div>"+
        "<div class='btn-group pull-right'>"+
        "<button type='button' id='confirmationYes' class='btn btn-xs btn-danger'><i class='glyphicon glyphicon-ok'></i>Yes</button>"+
        "<button type='button' class='btn btn-xs btn-default clear'><i class='glyphicon glyphicon-remove'></i>No</button>"+
        "</div>","DeleteConfirmation",{
            allowHtml:true,
            closeButton:true,
            onShow:function (){
                $("#confirmationYes").click(function () {
                    $.ajax({
                        type : "GET",
                        url  : "/springboot/" +type+ "/delete/"+id,
                        success : function (data) {
                            fetchList(type);
                            //alert(data);
                            toastr.success(data.message(),"Delete confirmation")
                        }
                    });
                });

            }

        });
}
*/
